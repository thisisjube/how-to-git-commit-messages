# How to Commit Messages

This repository contains a tutorial which introduces guidelines, tips and tricks for better git commit message to improve your git commit history.

## Basics

As you know, the commit command is used to save changes to a local repository after staging in Git. We use commit messages to describe those changes.

### Commit Options

After the <code>git add</code> command you can use several flags for <code>git commit</code>:
<br>
<br>
- <code>-m</code> Set the commit's message
```console
git commit -m "Your commit message"
```
- <code>-a</code> Commit all (including new) tracked, modified or deleted files
```console
git commit -a -m "Your commit message"
```
- <code>--amend</code> Rewrite the very last commit with any currently staged changes or a new commit message. Should only be performed on cimmts that have not been pushed to a remote repository, yet
```console
git commit --amend -m "Your new commit message"
```

### Command Line vs. Editor Method

Besides the command line method

    git commit -m "Subject" -m "Description"
    
you can also run <code>git commit</code> without a message or option and it'll open up your default text editor to write a commit message. In the following we will use this method.
### Extended Commit Message - Command Line Method

    git commit -m "Subject" -m "Description"

The first <code>-m</code> option is the subject (short description), and the next is the extended description (body).

## Guidelines

A good commit message should be structured like this:

    <type>[optional scope]: <Summarize change(s) in around 50 characters or less>

    [optional body: More detailed explanatory description of the change]

    [optional footer(s)]

<b>Commit Types</b>
- feat: The new feature you're adding to a particular application
- fix: A bug fix
- style: Feature and updates related to styling
- refactor: Refactoring a specific section of the codebase
- test: Everything related to testing
- docs: Everything related to documentation
- chore: Regular code maintenance

<b>Scope</b>
- A scope may be provided after a type. A scope must consist of a noun describing a section of the codebase surrounded by parenthesis, e.g., <code>fix(parser)</code>

<b>Summarized Description</b>
- A description must immediately follow the colon and space after the type/scope prefix. The description is a short summary of the code changes
- Use the imperative mood, e.g. <code>fix: Make time variable a double</code>
- Do not end the line with a period

<b>Body</b>
- If a body is provided, it must start with a blank line
- It can consist of several, newline seperated paragraphs
- Use the body to explain what changes you have made and why you made them.
- Do not assume the reviewer understands what the original problem was, ensure you add it.

<b>Footer</b>
- One or more footers may be provided one blank line after the body. Each footer must consist of a word token, followed by either a <br><code>:\<space></code> or <code>\<space>#</code> separator, followed by a string value. <br>
Example: <code>Fixes: #123</code>

## Sources

This guide was inspired by
- https://www.freecodecamp.org/news/writing-good-commit-messages-a-practical-guide/
- https://www.conventionalcommits.org/en/v1.0.0/
- https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

## To do

- [ ] Introduce breaking change keyword?
